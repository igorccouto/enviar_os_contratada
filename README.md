# Enviar Ordens de Serviço - Suporte Técnico

Código simples (específico) para ajudar fiscais no envio de ordens de serviço para a contratada.

----
## Antes de começar...
Certifique-se que o [GIT](https://git-scm.com/downloads) está instalado em seu PC. Proceda baixando o código fonte:

    $ git clone https://bitbucket.org/igorccouto/enviar_os_contratada
    $ cd enviar_os_contratada

### Configurações iniciais
1. Baixe e instale o [Python 3.7.4](https://www.python.org/downloads/release/python-374/).
    1. Testado no Windows 10 - 64 bits.
    2. Outras versões do Python podem funcionar.

2. Baixe e instale o [Google Chrome](https://www.google.com/intl/pt-BR/chrome) caso não possua.

3. Baixe o [ChromeDriver](http://chromedriver.chromium.org/downloads).
    1. Escolha de acordo com a versão do Google Chrome instalado no seu PC.
        1. Verifique a versão do Google Chrome instalada antes de baixar o ChromeDriver.
    2. Na raiz do projeto, crie uma pasta chamada *driver*.
    3. Renomeie o executável para *chromedriver.exe* e salve-o dentro de *driver*.

4. Para instalar a bibliotecas, dentro da pasta raiz, execute:

        $ pip install -r requirements.txt

### Arquivos de entrada

#### **.env**

No diretório raiz, crie um arquivo chamado *.env* com as seguintes variáveis:

- USUARIO=*SEU NOME DE USUARIO*
- SENHA=*SUA SENHA*

> **Por motivos de segurança, altere as permissões deste arquivo para que somente a sua conta tenha acesso de escrita/leitura.**

Não use aspas, colchetes ou qualquer caractere para delimitar as variáveis. Veja um modelo deste arquivo em [exemplos](https://bitbucket.org/igorccouto/enviar_os_contratada/src/master/exemplos/).

#### **config.ini**

No diretório raiz, crie um arquivo chamado *config.ini* contendo as informações dos relatórios que você deseja gerar. Veja um modelo deste arquivo em [exemplos](https://bitbucket.org/igorccouto/enviar_os_contratada/src/master/exemplos/).

## Uso
Após as configurações iniciais, no diretório raiz, execute:

    $ python src\executa.py

O código irá abrir um navegador e utilizará as informações em *config.ini* e *.env* para para exportar as ordens de serviço do portal.

Caso esteja no Windows, utilize o arquivo *executa.cmd* para criar um atalho na área de trabalho.

### Arquivo de saída
Ao término da execução, o um arquivo .csv será criado com as ordens de serviço.

> O repositório com as ordens de serviço criadas são de responsabilidade da regional. Para manter um registro, evite apagá-los.