import os
import re
import logging
import configparser
import pandas as pd
from os import listdir
from os.path import isfile, join
from datetime import datetime as dt, timedelta as td


def carrega_ini(arquivo):
    configuracao = configparser.ConfigParser()
    encodings = ['utf-8-sig', 'utf-8', 'latin-1']
    for encoding in encodings:
        try:
            configuracao.read(arquivo, encoding=encoding)
        except Exception:
            continue
        else:
            break
    
    return configuracao

def cria_registro(nome):
    registro = logging.getLogger(nome)
    registro.setLevel(logging.INFO)
    if not registro.hasHandlers():
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        formatter = logging.Formatter(
            '%(asctime)s - %(levelname)s - %(name)s - %(process)s - %(message)s'
        )
        handler.setFormatter(formatter)
        registro.addHandler(handler)
    return registro

def prepara_saida(diretorio, arquivo):
    diretorio = os.path.abspath(diretorio)
    
    if not os.path.exists(diretorio):
        os.mkdir(diretorio)

    saida = os.path.join(diretorio, arquivo)
    
    if os.path.exists(saida):
        os.remove(saida)

    return saida

def carrega_pkl(arquivo):
    if os.path.isfile(arquivo):
        sent_requests = pd.read_pickle(arquivo)
    else:
        config = carrega_ini('config.ini')
        FINAL_COLS = config['REQ_FILE']['COLUMNS'].split(',')
        sent_requests = pd.DataFrame(columns=FINAL_COLS)

    return sent_requests

def importa_reqs(arquivo):
    config = carrega_ini('config.ini')
    FINAL_COLS = config['REQ_FILE']['COLUMNS'].split(',')
    # Variáveis auxiliares
    dateparser = (lambda x: pd.datetime.strptime(x, '%Y/%m/%d %H:%M:%S'))
    parse_dates = ['Data Abertura', ]
    drop_columns = config['REQ_FILE']['DROP_COLS'].split(',')
    rename_columns = {}
    for i in config['REQ_FILE']['RENAME_COLS'].split(','):
        rename_columns.update({i.split(':')[0]: i.split(':')[1]})
    # Importando.
    requests = pd.read_csv(arquivo,
                           encoding='UTF-8',
                           delimiter=';',
                           dtype={'Pib': object},
                           parse_dates=parse_dates,
                           date_parser=dateparser)
    # Ajustes
    small_description = requests['Catop3'].str.len() < 30
    requests.loc[small_description, 'Catop3'] = (
        requests['Catop2'] + ' - ' + requests['Catop3']
    )
    requests['Organização Código de pesquisa'], requests['Cliente Organização'] = (
        requests['Mcu Org'].str.split(' - ', 1).str
    )
    requests['Local de Atendimento/Telefone para Contato'] = (
        requests['Cliente Organização']
    )
    requests['Descricao'] = requests['Descricao'].apply(
        lambda x: x.replace('\n', '. ')
    )
    requests['Descricao'] = requests['Descricao'].apply(
        lambda x: ' '.join(x.split())
    )
    requests['Descricao'] = requests['Descricao'].apply(lambda x: x[:250])
    # Remove colunas desnecessárias, renomeia as mantidas e ordena.
    requests = requests.drop(drop_columns, axis=1)
    requests = requests.rename(columns=rename_columns)
    cleaned_requests = requests[FINAL_COLS]
    sorted_requests = cleaned_requests.sort_values(
        by=['Data da atribuição'],
        ascending=False
    )
    sorted_requests['Data da atribuição'] = pd.to_datetime(
        sorted_requests['Data da atribuição']
    )
    return sorted_requests

def juntar_reqs(requests, sent_requests):
    notsent_requests = pd.merge(requests,
                                sent_requests,
                                on=['ID'],
                                how="outer",
                                indicator=True)
    left_only = notsent_requests['_merge'] == 'left_only'
    notsent_requests = notsent_requests[left_only]
    notsent_requests = notsent_requests.iloc[:, :11]
    for col in notsent_requests.columns:
        notsent_requests.rename(
            columns=lambda x: x.replace('_x', ''), inplace=True
        )
    # Adiciona 10 minutos em 'Data de atribuição'.
    notsent_requests['Data da atribuição'] = (
        dt.now() + td(minutes=10)
    )
    return notsent_requests

def proximo_arquivo(SENT_REQS, TODAY):
    sent_files = _getSentFiles(SENT_REQS, TODAY)
    next_letter = _getNextLetter(sent_files)
    return "{}\\OS_Contratante_{}{}.xls".format(SENT_REQS,
                                                TODAY.strftime('%d_%m_%y'),
                                                next_letter)

def _getSentFiles(SENT_REQS, TODAY):
    SENT_REQS_FILES = [
        f for f in listdir(SENT_REQS) if isfile(join(SENT_REQS, f))
    ]

    SAMEDAY_FILES = []
    for f in SENT_REQS_FILES:
        if re.search(TODAY.strftime('%d_%m_%y'), f) is not None:
            SAMEDAY_FILES.append(f)

    return SAMEDAY_FILES

def _getNextLetter(FILES):
    if not FILES:
        return 'A'
    LAST_FILE = FILES[-1]
    LAST_LETTER = LAST_FILE.split('.')[0][-1]
    return chr(ord(LAST_LETTER) + 1)

def estatisticas(df):
    months_names = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
                    'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro',
                    'Dezembro']
    df['data'] = df['Data da atribuição'].dt.strftime('%d/%m/%Y')
    df['mes'] = df['Data da atribuição'].apply(
        lambda date: months_names[date.month - 1]
    )
    df['ano'] = df['Data da atribuição'].dt.year
    por_ano_mes = df.groupby([(df.ano), (df.mes)]).size()
    por_ano_mes = por_ano_mes.iloc[::-1]
    por_data = df.groupby([(df.data), ]).size()
    por_data = por_data.iloc[::-1]
    print('')
    print('Quantidade de REQs enviadas por mês:')
    print(por_ano_mes.to_string())