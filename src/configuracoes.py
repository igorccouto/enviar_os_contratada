import os
from decouple import config
from suporte import carrega_ini, prepara_saida


# Credenciais dentro de .env
USUARIO = config('USUARIO')
SENHA = config('SENHA')

# Configurações dentro do arquivo .ini.
config = carrega_ini('config.ini')

DIRETORIO_SAIDA = os.path.abspath(config['SAP']['OUTPUT_DIR'])
arquivo_saida = '%s.csv' % config['SAP']['REPORT']
SAIDA_PORTAL = prepara_saida(diretorio=DIRETORIO_SAIDA, arquivo=arquivo_saida)

EXECUTAVEL = config['SAP']['DRIVER']
URL_INICIAL = config['SAP']['URL']
NUMERO_RELATORIO = config['SAP']['REPORT_NUM']
TITULO_RELATORIO = config['SAP']['REPORT']
NOME_GRUPO = config['SAP']['GROUP_NAME']
FAIXAS = config['SAP']['RANGES'].split(',')

BASE_DADOS = config['DEFAULT']['DATABASE']
ENVIADOS = config['DEFAULT']['SENT_REQS']
NOME_PLANILHA = config['REQ_FILE']['SHEETNAME']
