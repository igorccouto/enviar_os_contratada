from time import sleep
from selenium import webdriver
from suporte import cria_registro
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as ec
from configuracoes import (EXECUTAVEL, URL_INICIAL, NUMERO_RELATORIO, 
                           TITULO_RELATORIO, NOME_GRUPO, FAIXAS, USUARIO,
                           SENHA)

    
class Robo_SAPBO():
    def __init__(self):
        self.wait_time = 10
        self.registro = cria_registro('Robo SAPBO')

    def enable_download_in_headless_chrome(self, download_dir):
        self.driver.command_executor._commands['send_command'] = ('POST',
                                                                  '/session/$sessionId/chromium/send_command')
        params = {'cmd': 'Page.setDownloadBehavior',
                  'params': {'behavior': 'allow',
                             'downloadPath': download_dir}}
        self.driver.execute('send_command', params)

    def abrir_navegador(self, headless=False, download_dir='C:\\temp'):
        opt = webdriver.ChromeOptions()
        opt.add_argument('--start-maximized')
        prefs = {'download.default_directory': download_dir,
                 'download.prompt_for_download': False}
        opt.add_experimental_option('prefs', prefs)
        if headless:
            opt.add_argument('--headless')
            
        self.driver = webdriver.Chrome(executable_path=EXECUTAVEL,
                                       options=opt)
        
        if headless:
            self.enable_download_in_headless_chrome(download_dir=download_dir)
            
        self.action = ActionChains(self.driver)
            
    def pagina_inicial(self):
        self.registro.info('Acessando: %s' % URL_INICIAL)
        self.driver.get(URL_INICIAL)
        
    def logon(self):
        self.registro.info('Efetuando login como %s.' % USUARIO)
        # Always return to the default frame
        self.driver.switch_to.default_content()
        servletBridgeI_frame = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(servletBridgeI_frame)
        
        # Username input
        username_input = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//input[contains(@id, "logon:USERNAME")]'))
        )
        username_input.clear()
        username_input.send_keys(USUARIO)
        
        # Password input
        password_input = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//input[contains(@id, "PASSWORD")]'))
        )
        password_input.clear()
        password_input.send_keys(SENHA)
        
        # Clicking on "Logon" button
        wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//input[@value="Logon"]'))
        ).click()
    
    def logoff(self):
        self.registro.info('Efetuando logoff.')
        # Always return to the default frame
        self.driver.switch_to.default_content()
        servletBridgeI_frame = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(servletBridgeI_frame)
        
        # Clicking "Efetuar logoff" button
        wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.ID,
                                              'logoffLink-button'))
        ).click()
        sleep(1)
    
    def aba_relatorios(self):
        self.registro.info('Acessando a aba de relatórios.')
        # Always return to the default frame
        self.driver.switch_to.default_content()
        servletBridgeI_frame = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(servletBridgeI_frame)
        
        # Clicking on "Documentos" tab
        wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.LINK_TEXT, 'Documentos'))
        ).click()
        
        # Clicking on "Pastas"
        InfoViewAppActions_frame = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//iframe[contains(@src, "InfoViewAppActions")]'))
        )
        self.driver.switch_to.frame(InfoViewAppActions_frame)
        wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//a[text()="Pastas"]'))
        ).click()
        # Clicking on "Pastas Públicas"
        public_folder = wait(self.driver, self.wait_time).until(
                ec.visibility_of_element_located((
                    By.XPATH,
                    '//*[contains(text(), "Pastas públicas")]/parent::a'
        )))
        self.action.move_to_element(public_folder).perform()
        public_folder_id = public_folder.find_element_by_xpath('.//parent::td').get_attribute('id')
        self.driver.find_element_by_xpath('//*[@aria-labelledby="%s"]' % public_folder_id).click()
        # Clicking on "ITSM - SRM"
        itsm_srm = wait(self.driver, self.wait_time).until(
                ec.visibility_of_element_located((
                    By.XPATH,
                    '//*[contains(text(), "ITSM - SRM")]/parent::a'
        )))
        self.action.move_to_element(itsm_srm).perform()
        itsm_srm_id = itsm_srm.find_element_by_xpath('.//parent::td').get_attribute('id')
        self.driver.find_element_by_xpath('//*[@aria-labelledby="%s"]' % itsm_srm_id).click()
        # Clicking on "COORDENADOR / SUPERVISOR"
        supervisor = wait(self.driver, self.wait_time).until(
                ec.visibility_of_element_located((
                    By.XPATH,
                    '//*[contains(text(), "SUPERVISOR")]/parent::a'
        )))
        self.action.move_to_element(supervisor).perform()
        supervisor_id = supervisor.find_element_by_xpath('.//parent::td').get_attribute('id')
        self.driver.find_element_by_xpath('//*[@aria-labelledby="%s"]' % supervisor_id).click()
        # Clicking on "Detalhado"
        wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              '//*[contains(text(), "Detalhado")]/parent::a'))
        ).click()
        
    def selecionar_relatorio(self):
        """Select report based on Goes to the reports tab
        """
        self.registro.info('Selecionando o relatório %s.' % NUMERO_RELATORIO)
        xpath = '//*[starts-with(text(), "[%s] - ")]' % NUMERO_RELATORIO
        report = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.XPATH,
                                              xpath))
        )
        self.action.context_click(report).perform()
        wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.LINK_TEXT,
                                              'Programar'))
        ).click()
        
    def configura_titulo(self):
        self.registro.info('Configurando título como %s.' % TITULO_RELATORIO)
        # Always return to the default frame
        self.driver.switch_to.default_content()
        sB_frame = wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.XPATH,
                                            '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(sB_frame)
        dlg0_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer0"]'
        )
        self.driver.switch_to.frame(dlg0_frame)
        dlg_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer"]'
        )
        self.driver.switch_to.frame(dlg_frame)
        frame0 = self.driver.find_element_by_xpath(
            '//iframe[@id="frame0"]'
        )
        self.driver.switch_to.frame(frame0)
        title_input = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.ID,
                                              'instanceTitleForm:txtInstanceTitle'))
        )
        title_input.clear()
        title_input.send_keys(TITULO_RELATORIO)
        
    def configura_prompts(self):
        self.registro.info('Configurando grupo de trabalho %s.' % NOME_GRUPO)
        # Always return to the default frame
        self.driver.switch_to.default_content()
        sB_frame = wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.XPATH,
                                            '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(sB_frame)
        dlg0_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer0"]'
        )
        self.driver.switch_to.frame(dlg0_frame)
        dlgNav_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgNavFrame"]'
        )
        self.driver.switch_to.frame(dlgNav_frame)
        self.driver.find_element_by_link_text('Prompts').click()
        self.driver.switch_to.default_content()
        sB_frame = self.driver.find_element_by_xpath(
            '//iframe[@name="servletBridgeIframe"]'
        )
        self.driver.switch_to.frame(sB_frame)
        dlg0_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer0"]'
        )
        self.driver.switch_to.frame(dlg0_frame)
        dlg_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer"]'
        )
        self.driver.switch_to.frame(dlg_frame)
        frame2 = self.driver.find_element_by_xpath(
            '//iframe[@id="frame2"]'
        )
        self.driver.switch_to.frame(frame2)
        wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.XPATH,
                                            '//button[contains(text(), "Modificar")]'))
        ).click()
        group_name_input = wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located(
                (By.ID, 'lovWidgetpromptLovZone_textField')
            )
        )
        group_name_input.clear()
        group_name_input.send_keys(NOME_GRUPO)
        wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located(
                (By.ID, 'lrz_arrowAdd_promptLovZone_RightZone_basic'))
        ).click()

        self.registro.info('Configurando faixas: %s' % FAIXAS)
        # Faixa de Pendência
        self.driver.find_element_by_id('_CWpromptstrLstElt1').click()
        for r in FAIXAS:
            range_field = self.driver.find_element_by_id('lovWidgetpromptLovZone_textField')
            range_field.clear()
            range_field.send_keys(r)
            wait(self.driver, self.wait_time).until(
                ec.presence_of_element_located((By.ID,
                                                'lrz_arrowAdd_promptLovZone_RightZone_basic'))
            ).click()
        self.driver.find_element_by_id('RealBtn_OK_BTN_promptsDlg').click()
        # Waits...
        wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.XPATH,
                                            '//*[@title="%s"]' % NOME_GRUPO))
        )

    def configura_formato(self):
        self.registro.info('Configurando arquivo de saída.')
        # Always return to the default frame
        self.driver.switch_to.default_content()
        sB_frame = wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.XPATH,
                                            '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(sB_frame)
        dlg0_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer0"]'
        )
        self.driver.switch_to.frame(dlg0_frame)
        dlgNav_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgNavFrame"]'
        )
        self.driver.switch_to.frame(dlgNav_frame)
        wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.LINK_TEXT,
                                            'Formatos'))
        ).click()
        self.driver.switch_to.default_content()
        sB_frame = self.driver.find_element_by_xpath(
            '//iframe[@name="servletBridgeIframe"]'
        )
        self.driver.switch_to.frame(sB_frame)
        dlg0_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer0"]'
        )
        self.driver.switch_to.frame(dlg0_frame)
        dlg_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer"]'
        )
        self.driver.switch_to.frame(dlg_frame)
        frame3 = self.driver.find_element_by_xpath(
            '//iframe[@id="frame3"]'
        )
        self.driver.switch_to.frame(frame3)
        wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.ID,
                                            '_CWformatCheckListTWe_3'))
        ).click()
        delimiter_select = wait(self.driver, self.wait_time).until(
            ec.visibility_of_element_located((By.NAME,
                                              'csvColumnDelimiter'))
        )
        Select(delimiter_select).select_by_value(';')
    
    def agendar(self):
        self.registro.info('Clicando em "Agendar".')
        self.driver.switch_to.default_content()
        sB_frame = wait(self.driver, self.wait_time).until(
                    ec.presence_of_element_located((By.XPATH,
                                                    '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(sB_frame)
        dlg0_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer0"]'
        )
        self.driver.switch_to.frame(dlg0_frame)
        dlg_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer"]'
        )
        self.driver.switch_to.frame(dlg_frame)
        wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.ID,
                                            'actionsetform:asbtdone'))
        ).click()
    
    def aguardar_relatorio(self):
        self.driver.switch_to.default_content()
        sB_frame = wait(self.driver, self.wait_time).until(
            ec.presence_of_element_located((By.XPATH,
            '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(sB_frame)
        dlg0_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer0"]'
        )
        self.driver.switch_to.frame(dlg0_frame)

        table = wait(self.driver, 10).until(
            ec.presence_of_element_located((
                By.ID,
                'historyComponent_HistoryExplorer_detailView_listMainTable'))
        )

        row = table.find_element_by_xpath('//tr[contains(@aria-label, "%s")]' % USUARIO)

        id_prefix = row.get_attribute('id')
        id_status = id_prefix.replace('Node', 'Column_') + '_3'

        wait(self.driver, 180).until(
            ec.text_to_be_present_in_element((By.ID,
                                            id_status),
                                            'Êxito')
        )

    def baixar_relatorio(self):
        self.driver.switch_to.default_content()
        sB_frame = wait(self.driver, 2).until(
            ec.presence_of_element_located((By.XPATH,
            '//iframe[@name="servletBridgeIframe"]'))
        )
        self.driver.switch_to.frame(sB_frame)
        dlg0_frame = self.driver.find_element_by_xpath(
            '//iframe[@id="dlgContainer0"]'
        )
        self.driver.switch_to.frame(dlg0_frame)

        table = wait(self.driver, 3).until(
            ec.presence_of_element_located((By.ID,
                                        'historyComponent_HistoryExplorer_detailView_listMainTable'))
        )

        row = table.find_element_by_xpath('//tr[contains(@aria-label, "%s")]' % USUARIO)

        id_prefix = row.get_attribute('id')
        id_date = id_prefix.replace('Node', 'Column_') + '_1'
        report_date = self.driver.find_element_by_id(id_date).text
        self.registro.info('O relatório programado em %s está sendo baixado.' % report_date)

        id_download = id_prefix.replace('Node', 'Column_') + '_2'
        self.driver.find_element_by_id(id_download).find_element_by_xpath('./a').click()

    def sair(self):
        self.registro.info('Fechando navegador.')
        self.driver.quit()