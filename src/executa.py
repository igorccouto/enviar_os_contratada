import os
import pandas as pd
from time import sleep
from robos import Robo_SAPBO
from datetime import datetime
from win32com.client import Dispatch
from suporte import (cria_registro, carrega_pkl, importa_reqs, juntar_reqs,
                     proximo_arquivo, estatisticas)
from configuracoes import (DIRETORIO_SAIDA, SAIDA_PORTAL, BASE_DADOS, ENVIADOS,
                           NOME_PLANILHA)


if __name__ == '__main__':
    registro = cria_registro('executa')

    # Executa o robo
    robo = Robo_SAPBO()

    while True:
        print('---------')
        try:
            robo.abrir_navegador(download_dir=DIRETORIO_SAIDA)
            robo.pagina_inicial()
            robo.logon()
            robo.aba_relatorios()
            robo.selecionar_relatorio()
            robo.configura_titulo()
            robo.configura_prompts()
            robo.configura_formato()
            robo.agendar()
            robo.aguardar_relatorio()
            robo.baixar_relatorio()

            # Aguardando arquivo ser totalmente baixado
            while not os.path.exists(SAIDA_PORTAL):
                sleep(1)

            robo.logoff()
            robo.sair()
            break
        except Exception:
            registro.error('Um erro ocorreu! Tentaremos novamente.')
            robo.sair()

    # Carrega base de dados com chamados enviados anteriormente
    sent_requests = carrega_pkl(BASE_DADOS)

    # Relatório proviniente do - REQS oriundas do Portal de Relatórios
    requests = importa_reqs(SAIDA_PORTAL)

    # Filtra os chamados ainda não enviados
    notsent_requests = juntar_reqs(requests, sent_requests)

    if not notsent_requests.empty:
        next_filename = proximo_arquivo(ENVIADOS, datetime.now())
        # Salva o arquivo.
        writer = pd.ExcelWriter(next_filename,
                                datetime_format='dd/mm/yyyy hh:mm:ss',
                                date_format='dd/mm/yyyy')
        notsent_requests.to_excel(writer, NOME_PLANILHA, index=False)
        writer.save()
        # Abre o arquivo para verificação do fiscal.
        registro.info('Abrindo novo arquivo de chamados (%s)', next_filename)
        xl = Dispatch("Excel.Application")
        xl.Visible = True
        wb = xl.Workbooks.Open(next_filename)
        # Atualiza as requisições enviadas.
        registro.info('Salvando chamados enviados na base de dados (%s)', BASE_DADOS)
        sent_requests = pd.concat([notsent_requests, sent_requests])
        sent_requests.to_pickle(BASE_DADOS)
    else:
        registro.info('Não existem chamados a serem enviados em %s', SAIDA_PORTAL)

    os.remove(SAIDA_PORTAL)
    estatisticas(sent_requests)
    os.system('pause')